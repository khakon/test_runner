using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestRunner.Models
{
       public class ActiveCampaignSettings
    {
        public string Url { get; set; }
        public string EventKey { get; set; }
        public string EventUrl { get; set; }
        public string VisitUrl { get; set; }
        public string ApiKey { get; set; }
        public string ActId { get; set; }
        public string Email { get; set; }
        public string Host { get; set; }

        public ActiveCampaignSettings()
        {
            Url = "https://runnerrangeua.api-us1.com";
            EventKey = "790c7ee27c20aa965e69aee412b75e71397c3a8e";
            EventUrl = "http://trackcmp.net/event";
            VisitUrl = "http://trackcmp.net/visit";
            ApiKey = "36e8ba88e7bce6ad039e60f937ec5f345dbe4ff28ad99a707ad7b3a819c24976e90a6ce2";
            ActId = "89470654";
            Email = "tcfa@tcf.tcf";
            Host = "runner.range.com.ua";
        }
    }
}
