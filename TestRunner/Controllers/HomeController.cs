using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json.Linq;
using TestRunner.Models;

namespace TestRunner.Controllers
{
    public class HomeController : Controller
    {
        private IHttpContextAccessor _accessor;
        ActiveCampaignSettings _activeCampaignSettings = new ActiveCampaignSettings();
        public HomeController(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public IActionResult Registration()
        {
            return View();
        }
        [HttpGet]
        public IActionResult Download()
        {
            return View(new ProfileModel());
        }
        [HttpPost]
        public async Task<IActionResult> Download(ProfileModel model)
        {

            var result = await TrackProductDownloadAsync(model);
            if(result) ViewBag.Message = "Contact Added";
            else ViewBag.Message = "Downloaded";
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> AddContact(ProfileModel model)
        {
            var product = model.Product;
            var email = model.Email;
            bool isContactAdded = await AddContactAsync(email, product);
            if (isContactAdded) ViewBag.Message = "Contact Added";
            else ViewBag.Message = "Contact Added " + _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
            return View("Download");
        }

        [HttpPost]
        public async Task<IActionResult> AddEvent(ProfileModel model)
        {
            var product = model.Product;
            var email = model.Email;
            bool isEventAdded = await AddEventAsync(email, product);
            if (isEventAdded) ViewBag.Message = "Event Added";
            else ViewBag.Message = "Event Added";
            return View("Download");
        }

        [HttpPost]
        public async Task<IActionResult> AddVisit(ProfileModel model)
        {
            var product = model.Product;
            var email = model.Email;
            bool isVisitAdded = await AddVisitAsync(email);
            if (isVisitAdded) ViewBag.Message = "Visit Added";
            else ViewBag.Message = "Visit Added";
            return View("Download");
        }

        [HttpPost]
        public async Task<IActionResult> AddVisitTest(ProfileModel model)
        {
            var product = model.Product;
            var email = model.Email;
            bool isVisitAdded = await AddVisitTestAsync(email);
            if (isVisitAdded) ViewBag.Message = "Visit Added";
            else ViewBag.Message = "Visit Added";
            return View("Download");
        }

        private async Task<bool> TrackProductDownloadAsync(ProfileModel model)
        {
            var product = model.Product;
            var email = model.Email;
            bool isContactAdded = await AddContactAsync(email, product);
            bool isVisitAdded = await AddVisitAsync(email);
            if (isContactAdded)
            {
                return await AddEventAsync(email, product);
            }
            return isContactAdded;
        }

        private async Task<bool> AddEventAsync(string email, string encodedProduct)
        {
            using (var httpClient = new HttpClient { BaseAddress = new Uri(_activeCampaignSettings.EventUrl) })
            {
                string postData = $"event={WebUtility.UrlEncode("download_product")}&eventdata={encodedProduct}&actid={_activeCampaignSettings.ActId}" +
                    $"&key={_activeCampaignSettings.ApiKey}";
                QueryString apiQuery = new QueryString()
                .Add("track_actid", _activeCampaignSettings.ActId)
                .Add("track_key", _activeCampaignSettings.ApiKey)
                .Add("track_email", email);
                StringContent postDataContent = new StringContent(postData, Encoding.UTF8, "application/x-www-form-urlencoded");

                using (var response = await httpClient.PostAsync(httpClient.BaseAddress, postDataContent))
                {
                    string requestResult = await response.Content.ReadAsStringAsync();

                    var isEventSpawned = JObject.Parse(requestResult)["success"].Value<bool>();

                    return isEventSpawned;
                }
            }
        }

        
        private async Task<bool> AddVisitAsync(string email)
        {
            using (var httpClient = new HttpClient { BaseAddress = new Uri(_activeCampaignSettings.VisitUrl) })
            {
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36");
                httpClient.DefaultRequestHeaders.Add("Accept", "*/*");
                httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                httpClient.DefaultRequestHeaders.Add("Accept-Language", "en-US,en;q=0.9");
                StringBuilder builder = new StringBuilder();
                builder.Append(Request.Scheme + "://" + Request.Host + Request.Path.Value);
                QueryString apiQuery = new QueryString()
                    .Add("actid", _activeCampaignSettings.ActId)
                    .Add("r", Request.Headers["Referer"].ToString())
                    .Add("e", email)
                    .Add("u", builder.ToString());

                using (var response = await httpClient.GetAsync(_activeCampaignSettings.VisitUrl + apiQuery.ToString()))
                //using (var response = await httpClient.GetAsync("http://trackcmp.net/visit?actid=89470654&e=js@js.js&r=http%3A%2F%2Frunner.range.com.ua%2FHome%2FDownload&u=http%3A%2F%2Frunner.range.com.ua%2FHome%2FAddContact"))
                {
                    string requestResult = await response.Content.ReadAsStringAsync();
                    return true;
                }
            }
        }

        private async Task<bool> AddVisitTestAsync(string email)
        {
            using (var httpClient = new HttpClient { BaseAddress = new Uri(_activeCampaignSettings.VisitUrl) })
            {
                //js@js.js
                StringBuilder builder = new StringBuilder();
                builder.Append(Request.Scheme + "://" + _activeCampaignSettings.Host + Request.Path.Value);
                string postData = $"e={WebUtility.UrlEncode(_activeCampaignSettings.Email)}&r={WebUtility.UrlEncode("http://runner.range.com.ua/")}&u={WebUtility.UrlEncode("http://runner.range.com.ua/Home/AddContact")}&actid={_activeCampaignSettings.ActId}";
                httpClient.DefaultRequestHeaders.Add("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36");
                httpClient.DefaultRequestHeaders.Add("Accept", "*/*");
                httpClient.DefaultRequestHeaders.Add("Accept-Encoding", "gzip, deflate");
                httpClient.DefaultRequestHeaders.Add("Accept-Language", "en-US,en;q=0.9");
                string referrer = WebUtility.UrlEncode(Request.Headers["Referer"].ToString());
                using (var response = await httpClient.GetAsync(_activeCampaignSettings.VisitUrl + "?actid="+ _activeCampaignSettings.ActId + "&e=" + WebUtility.UrlEncode(email) + "&r="+ referrer + "&u=" + WebUtility.UrlEncode(builder.ToString())))
                {
                    string requestResult = await response.Content.ReadAsStringAsync();
                    return true;
                }
            }
        }

        private async Task<bool> AddContactAsync(string email, string encodedProduct)
        {
            using (var httpClient = new HttpClient { BaseAddress = new Uri(_activeCampaignSettings.Url) })
            {
                string firstName = email;
                string lastName = email;
                string ipClient = _accessor.HttpContext.Connection.RemoteIpAddress.ToString(); 
                string postData = $"email={email}&first_name=Test&last_name=Test&tags={encodedProduct}&ip4={ipClient}";

                QueryString apiQuery = new QueryString()
                    .Add("api_key", _activeCampaignSettings.ApiKey)
                    .Add("api_action", "contact_sync")
                    .Add("api_output", "json");

                StringContent postDataContent = new StringContent(postData, Encoding.UTF8, "application/x-www-form-urlencoded");

                using (var response = await httpClient.PostAsync($"/admin/api.php{apiQuery.ToString()}", postDataContent))
                {
                    string requestResult = await response.Content.ReadAsStringAsync();

                    var isContactAdded = JObject.Parse(requestResult)["result_code"].Value<bool>();

                    return isContactAdded;
                }
            }
        }
    }
}
